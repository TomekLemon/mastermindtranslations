<?php get_header(); ?>

	<div id="banner">
      <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('banner-subpage')) ?>
    </div><!-- /.carousel -->

	<div class="container">

   <!-- breadcrumbs -->
   <div class="row"><div class="container-fluid"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>
   <!-- /breadcrumbs -->

	<main role="main" id="subpage" class="search-results">
		<!-- section -->
		<section class="col-md-12">

			<h1><?php echo sprintf( __( '%s wyników dla frazy ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
