<?php /* Template Name: Contact */ get_header(); ?>

    <!-- Banner -->
    <div class="container banner-subpage">
     <?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	}  ?>
    <h1><?php the_title(); ?></h1>
    </div>
    <!-- /Banner -->
   
   <!-- breadcrumbs -->
   <div class="container"><div class="row"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>
   <!-- /breadcrumbs -->
   
	<main role="main" id="contact">
    
    <div class="container">
       
    <div class="row">
    
    
		<!-- section -->
		<section  class="col-md-6 col-sm-6">

		<h3><?php the_subtitle(); ?></h3>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
        
        <!-- sidebar -->
	<aside class="sidebar col-md-6 col-sm-6" role="complementary">
 	 <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('contact-sidebar')) ?>
    </aside>
	<!-- /sidebar -->
    
        </div>
        
        </div>
        
         
        
	</main>

<?php get_footer(); ?>
