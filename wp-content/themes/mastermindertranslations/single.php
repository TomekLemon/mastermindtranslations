<?php get_header(); ?>

	<!-- Banner -->
    <div class="container banner-subpage">
        <img src="<?php echo get_template_directory_uri(); ?>/img/blog-header.jpg" class="img-responsive" alt="Masterminder Translations Blog" />
    <h1>Blog</h1>
    </div>
    <!-- /Banner -->
    
    <div class="clearfix"></div>

   <!-- breadcrumbs -->
   <div class="row">
    <div class="container-fluid">
     <div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div>
    </div>
   </div>
   <!-- /breadcrumbs -->
   
   <div class="clearfix"></div>

	<main role="main" id="blog-post">
     <div class="container">
      <div class="row">
       
        <!-- sidebar -->
	    <aside class="sidebar col-md-3 col-sm-3" role="complementary">
 	     <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('blog-sidebar')) ?>
         
         <!-- Grey Boxes -->
         <h3>What can we help you with?</h3>
    <div class="row grey-boxes">
     
     <?php
       global $post;
       $args = array(
		'posts_per_page'   => 6,
		'offset'           => 0,
		'category'         => 5,
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
		);
       $myposts = get_posts( $args );
       foreach( $myposts as $post ) :  setup_postdata($post); ?>
      
      <div class="col-md-12">
       <div class="box-small">
	  <?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		the_post_thumbnail();
		} 
	  ?>
	  <h6 class="title"><?php the_title(); ?></h6>
      </div>
      </div>
      
      <?php endforeach; ?>
    </div>
    <!-- end Grey Boxes -->

    <ul class="can-i-help-menu hidden-xs">
     <li><a href="#contact-form">Yes, please</a></li>
     <li><a href="http://mt.huszal.com/how-it-works/">Not sure yet</a></li>
    </ul>
    
        </aside>
	    <!-- /sidebar -->
        
		<!-- section -->
		<section class="col-md-9 col-sm-9">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post title -->
			<h1><?php the_title(); ?></h1>
			<!-- /post title -->
            
            <!-- post date and author -->
            <div class="post-info">
 			 <div class="post-date"><div class="calendar-icon"></div><?php the_date('d M Y', '<span>', '</span>'); ?></div>
             <div class="post-author">by <?php the_author(); ?> / <?php comments_number( '0 COMMENTS', '1 COMMENT', '% COMMENTS' ); ?>. </div>
            </div>
            <!-- /post date and author -->
            
            <!-- post share btns -->
            <?php echo do_shortcode('[ssba]');?>
            <!-- /post share btns -->
            
            <!-- post image -->
            <div class="post-featured-image">
			 <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); }  ?>
            </div>
            <!-- /post image -->
            
            <!-- post content -->
			<div class="post-content"> 
			 <?php the_content(); // Dynamic Content ?>
             
             
            </div>
            <!-- /post content -->
            
            <div class="clearfix"></div>
            
            <?php dynamic_sidebar('related_posts'); ?>
            
            <!-- psot tags -->
            <div class="tags-box"><?php the_tags( '<ul class="tags"><li>', '</li><li>', '</li></ul>' ); ?></div>
            <!-- /post tags -->
            
            <div class="clearfix"></div>
            
            <!-- post share btns -->
            <?php echo do_shortcode('[ssba]');?>
            <!-- /post share btns -->
            
            
            <div class="clearfix"></div>
            
            <!-- comments -->
            <div class="comment list">
             <?php comments_template(); ?>
            </div>    
            <!-- /comments -->
            
             
            
            <!-- under the post -->
            <div class="about hidden-xs">
            <?php 
            $currentBlogPostId = get_the_ID();
            $blogPost = get_post($currentBlogPostId);
			$id = 125; 
            $mariusz_id = 2962;
            $sirena_id = 3148;
            if ($blogPost->post_author == 5) {
                $post = get_post($mariusz_id); 
            } elseif($blogPost->post_author == 6) {
                $post = get_post($sirena_id); 
            } else {
                $post = get_post($id); 
            }
			$content = apply_filters('the_content', $post->post_content); 
			
			echo '<div class="row">';
			echo '<div class="col-md-2 col-sm-2">';
			the_post_thumbnail();
			echo '</div>';
			
			echo '<div class="col-md-10 col-sm-10">';
			echo '<h4>';
			the_title();
			echo '</h4>';
			echo $content;  
			echo '</div>';
			echo '</div>';
			?>
            </div>
            <!-- /under the post -->
            
            <div class="clearfix"></div>     
            
              
			
		</article>
		<!-- /article -->



	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->



	<?php endif; ?>
    
   


		</section>
		<!-- /section -->
        
        
        
        </div>
      </div>  
	</main>

<?php get_footer(); ?>