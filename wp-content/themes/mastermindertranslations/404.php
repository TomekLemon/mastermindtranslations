<?php get_header(); ?>

	<div id="banner">
      <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('banner-subpage')) ?>
    </div><!-- /.carousel -->

	<div class="container">

   <!-- breadcrumbs -->
   <div class="row"><div class="container-fluid"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>
   <!-- /breadcrumbs -->

	<main role="main" id="subpage">
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404">

				<h1><?php _e( 'Podana strona nie istnieje!', 'html5blank' ); ?></h1>
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Powrót do strony głównej?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
