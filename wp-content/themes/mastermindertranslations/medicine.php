<?php /* Template Name: Medicine */ get_header(); ?>

    <!-- Banner -->
    <div class="container banner-subpage">
     <?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	}  ?>
    <h1>Sectors</h1>
    </div>
    <!-- /Banner -->
   
   <!-- breadcrumbs -->
   <div class="container"><div class="row"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>
   <!-- /breadcrumbs -->
   
	<main role="main" id="about">
    
    <div class="container">
    
    <h2><?php the_title(); ?></h2>
    
    <div class="row">
    
    <!-- sidebar -->
	<aside class="sidebar col-md-3 col-sm-3" role="complementary">
 	 <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sectors-sidebar')) ?>
    </aside>
	<!-- /sidebar -->
    
		<!-- section -->
		<section  class="col-md-9 col-sm-9 subpage-section">

			

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
        </div>
        
        </div>
        
         <!-- Testimonials -->
    <div class="testimonials-title-sub container">
     <h2>Testimonials</h2>
    </div>
    
    <div class="container">
   <ul class="testimonials-list clearfix">     
    <?php
       global $post;
       $args = array(
		'posts_per_page'   => '4',
		'offset'           => 0,
		'category'         => 15,
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
		);
       $myposts = get_posts( $args );
       foreach( $myposts as $post ) :  setup_postdata($post); ?>
    
   
     <li class="col-md-3">
        <h6 class="title"><?php the_title(); ?></h6>
        <p><?php the_content(); ?></p>
     </li>
    <?php endforeach; ?>
    <div class="clearfix"></div>
    <a href="<?php echo get_category_link(6); ?>" class="see-more-testimonials-filled">Read more</a>
    </ul>
    </div>
    </div>
    </div>
    </div>
    <!-- end Testimonials -->
        
	</main>

<?php get_footer(); ?>
