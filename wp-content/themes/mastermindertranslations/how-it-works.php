<?php /* Template Name: How It Works */ get_header(); ?>



    <!-- Banner -->

    <div class="container banner-subpage hiw-banner">

     <?php if ( has_post_thumbnail() ) {

		the_post_thumbnail();

	}  ?>

    <h1><?php the_title(); ?></h1>

    </div>

    <!-- /Banner -->

   

   <!-- breadcrumbs -->

   <div class="container"><div class="row"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>

   <!-- /breadcrumbs -->

   

	<main role="main" id="hiw">

    

    <div class="container">

    

    <h2><?php the_subtitle(); ?></h2>

       

    <div class="row">

    

    <!-- sidebar -->

	<aside class="sidebar col-md-4 hidden-sm hidden-xs" role="complementary">

    <!-- Grey Boxes -->

         <h3>What can we help you with?</h3>

    <div class="row grey-boxes">

     

     <?php

       global $post;

       $args = array(

		'posts_per_page'   => 6,

		'offset'           => 0,

		'category'         => 5,

		'category_name'    => '',

		'orderby'          => 'date',

		'order'            => 'ASC',

		'include'          => '',

		'exclude'          => '',

		'meta_key'         => '',

		'meta_value'       => '',

		'post_type'        => 'post',

		'post_mime_type'   => '',

		'post_parent'      => '',

		'author'	   => '',

		'post_status'      => 'publish',

		'suppress_filters' => true 

		);

       $myposts = get_posts( $args );

       foreach( $myposts as $post ) :  setup_postdata($post); ?>

      

      <div class="col-md-12">

       <div class="box-small">

	  <?php 

		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.

		the_post_thumbnail();

		} 

	  ?>
           <h5>Polish</h5>
	  <h6 class="title"><?php the_title(); ?></h6>

      </div>

      </div>

      

      <?php endforeach; ?>

    </div>

    <!-- end Grey Boxes -->

 	 <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('hiw-sidebar')) ?>

    </aside>

	<!-- /sidebar -->

    

		<!-- section -->

		<section  class="col-md-8 col-sm-12 col-xs-12 subpage-section">



			



		<?php if (have_posts()): while (have_posts()) : the_post(); ?>



			<!-- article -->

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



				<?php the_content(); ?>






			</article>

			<!-- /article -->



		<?php endwhile; ?>



		<?php else: ?>



			<!-- article -->

			<article>



				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>



			</article>

			<!-- /article -->



		<?php endif; ?>



		</section>

		<!-- /section -->

        </div>

        

        </div>

        

        <!-- Contact Form -->

    <div class="contact-form collapse" id="collapse-form"><a name="contact-form"></a>

     <div class="container"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('contact-form-about')) ?></div>

    </div>

    <!-- end Contact Form -->

         

        

	</main>



<?php get_footer(); ?>

