		</div>
		<!-- /main -->
        <!-- footer -->
		<footer class="footer" role="contentinfo">
         <div class="row">
          <div class="container">
          <!-- footer can1 -->
           <div class="footer-can1 col-md-4 col-sm-6 col-xs-12">
           <img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png" alt="Polish Translator" class="img-responsive">
           <div class="clearfix"></div>
           <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-can1')) ?>
           </div>
          <!-- end footer can1 -->
          <!-- footer can2 -->
           <div class="footer-can2 col-md-4 col-sm-6 col-xs-12"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-can2')) ?></div>
          <!-- end footer can2 -->
          <!-- footer can3 -->
           <div class="footer-can3 col-md-4 col-sm-12 col-xs-12"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-can3')) ?></div>
          <!-- end footer can3 --> 
          <!-- Copyrights -->
          <div class="footer-can4 col-md-12"> 
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-sm-12 col-xs-12">
                  <div class="copyright">Copyright &copy; 2017 <span>Mastermind Translations Ltd</span>. All Rights Reserved.</div>
              </div>
             <div class="col-md-6 col-sm-12 col-xs-12 by-who">
              <div class="design pull-right"><span>Design by:</span> <a href="http://www.lemonadestudio.pl" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/lemonade-logo.png" alt="Lemonade Studio" class="img-responsive"></a></div>
             </div>
             </div>
          </div>
           </div>
           </div>
          </div>    
         </div>
		</footer>
		<!-- /footer -->
        <!-- Bootstrap core JavaScript
	    ================================================== -->
    	<!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<?php wp_footer(); ?>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/lib/conditionizr-4.3.0.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-hover-dropdown.min.js"></script>
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="<?php echo get_template_directory_uri(); ?>/js/ie10-viewport-bug-workaround.js"></script>  
		<!-- analytics -->
        <script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
	</body>
</html>

