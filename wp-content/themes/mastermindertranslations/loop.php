<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<!-- article -->
        <article class="post-article" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="col-md-12">
            <!-- post title -->
            <a class="post-title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            <!-- /post title -->

            <div class="clearfix"></div>

            <!-- post details -->
            <span class="date"><?php the_time('j F, Y'); ?></span>
            <!-- /post details -->
            <?php
               if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                    the_post_thumbnail();
            } ?>

            <?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>

            </div>
            <div class="clearfix"></div>

        </article>
        <!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
