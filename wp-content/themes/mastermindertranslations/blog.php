<?php /* Template Name: Blog */ get_header(); ?>

    <!-- Banner -->
    <div class="container banner-subpage">
     <?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	}  ?>
    <h1><?php the_title(); ?></h1>
    </div>
    <!-- /Banner -->
   
   <!-- breadcrumbs -->
   <div class="container"><div class="row"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>
   <!-- /breadcrumbs -->
   
	<main role="main" id="blog-post">
    
    <div class="container">
       
    <div class="row">
    
    <!-- sidebar -->
	    <aside class="sidebar col-md-4 col-sm-3" role="complementary">
 	     <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('blog-sidebar')) ?>
         
         <!-- Grey Boxes -->
         <h3 class="hidden-xs">What can we help you with?</h3>
    <div class="row grey-boxes hidden-xs">
     
     <?php
       global $post;
       $args = array(
		'posts_per_page'   => 6,
		'offset'           => 0,
		'category'         => 5,
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
		);
       $myposts = get_posts( $args );
      ?>
    <div class="col-md-12">
        <a href="https://www.mastermindtranslations.co.uk/sectors/polish-medical-translation/">
            <div class="box-small">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon3.png" alt="alt" />
                <h5>Polish</h5>
                <h6 class="title">Medical Translation</h6>
            </div>
        </a>
        <a href="https://www.mastermindtranslations.co.uk/sectors/polish-pharmaceutical-translation/">
            <div class="box-small">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon2.png" alt="alt" />
                <h5>Polish</h5>
                <h6 class="title">Pharamaceutical Translation</h6>
            </div>
        </a>
        <div class="box-small">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon1.png" alt="alt" />
            <h5>Polish</h5>
            <h6 class="title">Chemical Translation</h6>
        </div>
        <a href="https://www.mastermindtranslations.co.uk/sectors/polish-biotechnology-translation/">
            <div class="box-small">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon6.png" alt="alt" />
                <h5>Polish</h5>
                <h6 class="title">Biotechnology Translation</h6>
            </div>
        </a>
        <div class="box-small">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon4.png" alt="alt" />
            <h5>Polish</h5>
            <h6 class="title">Patent Translation</h6>
        </div>
        <div class="box-small">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon5.png" alt="alt" />
            <h5>Polish</h5>
            <h6 class="title">Regulatory Affairs Translation</h6>
        </div>
    </div>
      
    </div>
    <!-- end Grey Boxes -->

    <ul class="can-i-help-menu">
     <li><a href="https://www.mastermindtranslations.co.uk/polish-translators-contact-details/">Yes, please</a></li>
     <li><a href="https://www.mastermindtranslations.co.uk/tlumaczenia-medyczne/">Not sure yet</a></li>
    </ul>
    
        </aside>
	    <!-- /sidebar -->
    
		<!-- section -->
		<section  class="col-md-8 col-sm-9">

<?php

  global $paged;
  global $queryObject;
  $temp = $queryObject; 
  $queryObject = null; 
  
$queryObject = new  Wp_Query( array(
    'posts_per_page'   => 116,
    'post_type' => array('post'),
    'category_name'         => blog,
    'orderby' => 1,
	'paged' => $paged
	
    ));





// The Loop
if ( $queryObject->have_posts() ) :
    $i = 0;
    while ( $queryObject->have_posts() ) :
        $queryObject->the_post();
        if ( $i == 0 ) : ?>
       
       <!-- First post -->
        <div class="first-post blog-post post-box">
         <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-12">
           <h4 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
           <div class="post-info"> 			 
             <div class="post-author">by <?php the_author(); ?> / <?php comments_number( '0 COMMENTS', '1 COMMENT', '% COMMENTS' ); ?>. </div>
           </div>
      
	    <?php html5wp_excerpt('html5wp_index') ?>
        <div class="clearfix"></div>
 
        <!-- post tags -->
       <div class="tags-box"><?php the_tags( '<ul class="tags"><li>', '</li><li>', '</li></ul>' ); ?></div>
       <!-- /post tags -->
       </div>
       
       <div class="col-md-4 col-sm-4 col-xs-12">
	   <div class="post-date"><div class="calendar-icon"></div><?php the_date('d M Y', '<span>', '</span>'); ?></div>
	   <?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		the_post_thumbnail();
		} 
	  ?></div>
      
      </div>      
      </div>    
      <!-- /First post -->  
      
        <?php endif;
        if ( $i != 0 ) : ?>

        
        <div class="blog-post post-box">
      
	  <h4 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
      
      <!-- post date and author -->
            <div class="post-info">
 			 <div class="post-date"><div class="calendar-icon"></div><?php the_date('d M Y', '<span>', '</span>'); ?></div>
             <div class="post-author">by <?php the_author(); ?> / <?php comments_number( '0 COMMENTS', '1 COMMENT', '% COMMENTS' ); ?>. </div>
            </div>
      <!-- /post date and author -->
      
      <div class="row">
       <div class="col-md-2 col-sm-2 hidden-xs"><?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		the_post_thumbnail();
		} 
	  ?></div>
       <div class="col-md-10 col-sm-10 col-xs-12">
	    <?php html5wp_excerpt('html5wp_index') ?>
        <div class="clearfix"></div>
 
        <!-- post tags -->
       <div class="tags-box"><?php the_tags( '<ul class="tags"><li>', '</li><li>', '</li></ul>' ); ?></div>
       <!-- /post tags -->
       </div>
      </div>
        </div>
        
        <?php endif; ?>
        <?php $i++; ?>
    <?php endwhile; ?>
  
 <?php get_template_part('pagination'); ?>
<?php endif; ?>


    <?php previous_posts_link('&laquo; Newer') ?>
    <?php next_posts_link('Older &raquo;') ?>

<?php 
  $queryObject = null; 
  $queryObject = $temp; 
?>

		</section>
		<!-- /section -->
        

        </div>
        
        </div>
        
         
        
	</main>

<?php get_footer(); ?>
