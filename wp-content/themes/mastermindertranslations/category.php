<?php get_header(); ?>

	<main role="main" class="clearfix container testimonials-subpage" id="subpage">
		<!-- section -->
        <!-- Banner -->
        <div class="container banner-subpage">
            <img src="<?php echo get_template_directory_uri(); ?>/img/testimonials-slider.jpg" alt="alt" />
            <h1><?php single_cat_title(); ?></h1>
        </div>
        <!-- /Banner -->
		<section>


			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
