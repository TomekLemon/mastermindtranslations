<?php get_header(); ?>

	<div id="banner">
      <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('banner-subpage')) ?>
    </div><!-- /.carousel -->

	<div class="container">

   <!-- breadcrumbs -->
   <div class="row"><div class="container-fluid"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>
   <!-- /breadcrumbs -->

	<main role="main" id="subpage">
		<!-- section -->
		<section class="col-md-12">

			<h1><?php the_title(); ?></h1>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
