<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
        <!-- Bootstrap 3.3.5 -->
	    <link href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oxygen:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
        <!-- Custom styles for this template -->
        <link href="<?php echo get_template_directory_uri(); ?>/css/carousel.css" rel="stylesheet">
	    <link href="<?php echo get_template_directory_uri(); ?>/style.css?ver=7" rel="stylesheet">
	</head>
  <body <?php body_class(); ?>>
    <!-- Top Line Bar -->
    <div class="top-line-bar hidden-xs">
        <div class="container">
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('top-line-bar')) ?>
        </div>
    </div>
    <!-- /Top Line Bar -->
    <div class="clearfix"></div>
    <!-- Navbar -->
    <div class="nav-bar">
        <div class="container">
            <div class="navbar" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
              <!-- logo -->
              <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/mastermind-translations-logo.png" alt="Mastermind Translations" class="img-responsive"></a>
              <!-- /logo -->
            </div>
            <!-- main menu -->
            <div class="navbar-collapse collapse">
			<?php html5blank_nav(); ?>
            </div>
            <!-- /main menu -->
          </div>
        </div>
      </div>
    </div>
    <!-- /Navbar -->

	 