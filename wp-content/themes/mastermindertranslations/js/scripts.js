(function ($, root, undefined) {
    $(function() {
//        $('.dropdown-toggle').toggleNavDropdown();
        $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    
    });

    (jQuery, this);

    $('.nav-tabs a').on('click', function (e) {
        $(this).tab('show');
        e.preventDefault();
    ``    });  

    $('.carousel').carousel({
        interval: 1000 * 15
    });

    $('#question-logic .choose-your-destiny .yes').click(function(e) {
        $(this).toggleClass('select');
        e.preventDefault();
    });

    $('#question-logic .choose-your-destiny .no').click(function(e) {
        $(this).toggleClass('select');
        e.preventDefault();
    });

    $('label.accept span').click(function(e) {
        $(this).toggleClass('selected');
        e.preventDefault();
    });

    document.getElementById("file-upload").onchange = function() {
        if (this.selectedIndex !== 0) {
            window.location.href = this.value;
    }
};
});
$(function() {
    $('#menu-item-18').unbind().on('click', function(e) {
        $('#menu-item-18 .dropdown-menu').fadeToggle();
    });
    
    $('a').on('click', function(e) {
        var address = $(this).attr('href');
            if (address === 'https://www.mastermindtranslations.co.uk/sectors/') {
                e.preventDefault();
                $(this).css('cursor', 'default');
                $(this).css('text-decoration', 'none');
            }
    });
});
