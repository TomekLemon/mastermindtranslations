<?php /* Template Name: About */ get_header(); ?>

    <!-- Banner -->
    <div class="container banner-subpage">
     <?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	}  ?>
    <h1><?php the_title(); ?></h1>
    </div>
    <!-- /Banner -->
   
   <!-- breadcrumbs -->
   <div class="container"><div class="row"><div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?></div></div></div>
   <!-- /breadcrumbs -->
   
	<main role="main" id="about">
    
    <div class="container">
    
    <h2><?php the_subtitle(); ?></h2>
    
    <div class="row">
    
    <!-- sidebar -->
	<aside class="sidebar col-md-3 col-sm-12" role="complementary">
 	 <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('about-sidebar')) ?>
    </aside>
	<!-- /sidebar -->
    
		<!-- section -->
		<section  class="col-md-9 col-sm-12">

			

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
        </div>
        
        <div class="fat-line hidden-xs"></div>
        
        </div>
        
        <!-- Qualifications Title --
        <h2 class="qualifications-title hidden-xs">Qualifications</h2>
        
        <div class="row testimonials hidden-xs">
        <div class="container">
         <div class="tab-content">
  
       <?php
       global $post;
       $args = array(
		'posts_per_page'   => 4,
		'offset'           => 0,
		'category'         => 14,
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
		);
       $myposts = get_posts( $args );
       foreach( $myposts as $post ) :  setup_postdata($post); ?>
       

     <div role="tabpanel" class="tab-pane fade in <?php if($counter==0) { echo 'active'; $counter++; } ?>" id="home-<?php echo $counter; $counter++; ?>">
        <p><?php the_content(); ?></p>
     </div>
    

    <?php endforeach; ?>
</div>

  <a class="special-arrow-btn" href="#">What does this<br />means for you?</a>

  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
     <a class="first" href="#home-1" aria-controls="home-1" role="tab" data-toggle="tab">LANGUAGE &amp;<br />TRANSLATION</a>
    </li>
    <li role="presentation">
     <a class="second" href="#home-2" aria-controls="home-2" role="tab" data-toggle="tab">MEDICINE &amp;<br />PHARMACEUTICALS</a>
    </li>
    <li role="presentation">
     <a class="third" href="#home-3" aria-controls="home-3" role="tab" data-toggle="tab">TECHNOLOGY &amp;<br />PATENTS</a>
    </li>
    <li role="presentation">
     <a class="fourth" href="#home-4" aria-controls="home-4" role="tab" data-toggle="tab">EXPORT OF<br />PRODUCTS &amp;<br />SERVICES</a>
    </li>
  </ul>

</div>
</div>
        
        
        
        <!-- Contact Form -->
    <div class="contact-form about">
     <a name="contact-form"></a>
     <div class="container"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('contact-form-about')) ?></div>
    </div>
    <!-- end Contact Form -->
        
	</main>

<?php get_footer(); ?>
