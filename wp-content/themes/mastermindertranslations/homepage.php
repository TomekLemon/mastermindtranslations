<?php /* Template Name: Homepage */ get_header(); ?>


   <!-- main -->

   <!-- content -->
   <main role="main" id="homepage">
	<!-- section -->
    <div class="row">
    <div class="container">
	<section class="col-md-12 banner">
     
     
	 <?php if (have_posts()): while (have_posts()) : the_post(); ?>
     <?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	}  ?>

	 <!-- article -->
     <div class="homepage-banner">
	 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	  <?php the_content(); ?>
      <div class="clearfix"></div>
      <!-- Banner Menu -->
      <div id="banner-menu">
       <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('banner-menu')) ?>
      </div>
      <!-- /Banner Menu -->
	 </article>
     </div>
	 <!-- /article -->

	 <?php endwhile; ?>
     <?php else: ?>

	 <!-- no article -->
	 <article>
      <h2><?php _e( 'Sorry, nothing to see here...', 'html5blank' ); ?></h2>
     </article>
	 <!-- /no article -->

	 <?php endif; ?>

	</section>
    </div>
	<!-- /section -->
    <div class="clearfix"></div>
    
    </div>
    
    <!-- Blue Bar -->
    <div class="blue-bar">
     <h1>Let’s start with you</h1>
     <h3>Do you need...</h3>
    </div>
    <!-- end Blue Bar -->
    
    <!-- Grey Boxes -->
    <div class="row grey-boxes">
     <div class="container">
    
     
     <?php
       global $post;
       $args = array(
		'posts_per_page'   => 6,
		'offset'           => 0,
		'category'         => 5,
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
		);
       $myposts = get_posts( $args );
      ?>
      <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="https://www.mastermindtranslations.co.uk/sectors/polish-medical-translation/">
            <div class="box">
                <div class="inside-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon3.png" alt="alt" />
                    <h5>Polish</h5>
                    <h6 class="title">Medical Translation</h6>
                </div>
            </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="https://www.mastermindtranslations.co.uk/sectors/polish-pharmaceutical-translation/">
            <div class="box">
                <div class="inside-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon2.png" alt="alt" />
                    <h5>Polish</h5>
                    <h6 class="title">Pharamaceutical Translation</h6>
                </div>
            </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box">
                <div class="inside-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon1.png" alt="alt" />
                    <h5>Polish</h5>
                    <h6 class="title">Chemical Translation</h6>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="https://www.mastermindtranslations.co.uk/sectors/polish-biotechnology-translation/">
            <div class="box">
                <div class="inside-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon6.png" alt="alt" />
                    <h5>Polish</h5>
                    <h6 class="title">Biotechnology Translation</h6>
                </div>
            </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box">
                <div class="inside-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon4.png" alt="alt" />
                    <h5>Polish</h5>
                    <h6 class="title">Patent Translation</h6>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box">
                <div class="inside-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon5.png" alt="alt" />
                    <h5>Polish</h5>
                    <h6 class="title">Regulatory Affairs Translation</h6>
                </div>
            </div>
        </div>
      </div>
      
      </div>
    </div>
    <!-- end Grey Boxes -->
    
    
    <!-- Testimonials -->
    <div class="testimonials-title container">
     <h2>Nearly <span>300</span> clients like you</h2>
     <h2>have already used our services.</h2>
     <h4>Here’s what they say:</h4>
    </div>
    
    <div class="row testimonials">
    <div class="container">
    
     <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
     <div class="carousel-inner" role="listbox">
     
    <?php
       global $post;
       $args = array(
		'posts_per_page'   => '2',
		'offset'           => 0,
		'category'         => 37,
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
		);
       $myposts = get_posts( $args );
       foreach( $myposts as $post ) :  setup_postdata($post); ?>
       

        <div class="col-md-6 col-sm-6">
        
        <p><?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		the_post_thumbnail();
		} 
	  ?><?php the_content(); ?></p>
      <h6 class="title"><?php the_title(); ?></h6>
        </div>
    
   
    <!--  <div class="item <?php if($counter==0) { echo 'active'; $counter++; } ?>">
      
      <div class="carousel-caption">
      
        <h6 class="title"><?php the_title(); ?></h6>
        <p><?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		the_post_thumbnail();
		} 
	  ?><?php the_content(); ?></p>
      </div>
    </div> -->
    <?php endforeach; ?>
    
    </div>
    </div>
    <a href="<?php echo get_category_link(6); ?>" class="see-more-testimonials">Read more</a>
    </div>
    </div>
    <!-- end Testimonials -->
    
    <!-- Contact Form -->
    <div class="contact-form"><a name="contact-form"></a>
     <div class="container"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('contact-form')) ?></div>
    </div>
    <!-- end Contact Form -->
    
	</main>

<?php get_footer(); ?>

