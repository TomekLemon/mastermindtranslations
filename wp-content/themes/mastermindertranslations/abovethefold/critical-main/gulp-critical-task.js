/**
 * WordPress Gulp.js Critical CSS Task Package
 *
 * This file contains a task to create critical path CSS.
 *
 * @warning This task automatically updates WordPress Critical CSS.
 * @criticalcss global.css
 *
 * @package    abovethefold
 * @subpackage abovethefold/modules/critical-css-build-tool
 * @author     PageSpeed.pro <info@pagespeed.pro>
 */
module.exports = function (gulp, plugins, critical) {
    return function (cb) {

    	var taskname = "critical-main";
    	var taskpath = taskname + '/';

    	// check if html file exists in package
    	if (!plugins.fs.existsSync(taskpath + 'page.html')) {
    		throw new Error('page.html does not exist in package');
            return false;
        }

    	// check if full css file exists in package
    	if (!plugins.fs.existsSync(taskpath + 'full.css')) {
    		throw new Error('full.css does not exist in package');
            return false;
        }

        var extraCSS = false;

    	// check if extra css file exists in package
    	if (plugins.fs.existsSync(taskpath + 'extra.css')) {
    		extraCSS = true;
        }

		// check if css file exists in package
    	if (plugins.fs.existsSync('css/' + "global.css")) {

    		// extract config comment to preserve on auto update
    		var csscontent = plugins.fs.readFileSync('css/' + "global.css", 'utf-8');
			var match = csscontent.match(/\/\*\*(.|\n)+?\*\//g);
			if (!match || !match[0]) {
				throw new Error("global.css" + ' does not contain a valid config header.');
            	return false;
			}
    		var configheader = match[0] + '\n';
        } else {
        	var configheader = '/*\n * '+"global.css"+'\n */\n';
        }

        // optimization tasks
        var TASKS = {};

        // Clean output directory
        TASKS['clean'] = function() {
        	return new Promise(function(resolve, reject) {

				console.log('\nCleaning output directory', plugins.util.colors.red.bold('/'+taskpath+'output/'),'...');

				gulp.src([taskpath + 'output'], { read: false })
        			.pipe(plugins.clean())
					.on('error', reject)
					.on('data', function () {}) 
					.on('end', resolve);

			});
        };

        // create citical CSS
        TASKS['critical'] = function() {

        	console.log('\n' + plugins.util.colors.yellow.bold('Creating Critical Path CSS...'));

			/**
	    	 * Perform critical CSS generation
	    	 * @link https://github.com/addyosmani/critical
	    	 */
	    	return critical.generate({
		        inline: false, // generate
		        base: taskpath ,
		        src: 'page.html',
		        dest: 'output/critical.css',
		        minify: false,
				css: [taskpath + "\/css\/1-www-mastermindtranslations-co-uk-normalize.css",taskpath + "\/css\/2-www-mastermindtranslations-co-uk-style.css",taskpath + "\/css\/3-www-mastermindtranslations-co-uk-jetpack.css",taskpath + "\/css\/4-www-mastermindtranslations-co-uk-bootstrap.min.css",taskpath + "\/css\/5-fonts-googleapis-com-css",taskpath + "\/css\/6-www-mastermindtranslations-co-uk-carousel.css",taskpath + "\/css\/7-www-mastermindtranslations-co-uk-style.css",taskpath + "\/css\/8-157755966aa52074af8b278967da607d.css",taskpath + "\/css\/9-86d3b6f41de47a4e59af082f409b3dc6.css",taskpath + "\/css\/10-185f24f65f56b26adfc1ea7d058cd977.css",taskpath + "\/css\/11-dd250e187b91917f6a91560e44bf1fd8.css"],
				extract: false,
				width: 1300,
				height: 900,
				pathPrefix: '../../../../', // wordpress root from /themes/THEME-NAME/abovethefold/
				timeout: 120000
		    });
        };

        // concatenate extra.css
        TASKS['concat'] = function() {
			return new Promise(function(resolve, reject) {

				if (!extraCSS) {
					resolve();
					return;
				}

				console.log(plugins.util.colors.white.bold(' ➤ Append extra.css to critical.css...'));

				// append extra.css
				gulp.src([taskpath + 'output/critical.css', taskpath + 'extra.css'])
				    .pipe(plugins.concat('critical+extra.css'))
			        .pipe(gulp.dest(taskpath + 'output'))
	    			.on('error', reject)
			        .on('end', resolve);

			});
        };

        // minify
        TASKS['minify'] = function() {
        	return new Promise(function(resolve, reject) {

				console.log(plugins.util.colors.white.bold(' ➤ Minify critical CSS...'));

				// append extra.css
				gulp.src(['!*.min.css',taskpath + 'output/*.css'])
					.pipe(plugins.cssmin({
			            "keepSpecialComments": false,
			            "advanced": true,
			            "aggressiveMerging": true,
			            "showLog": true
					})).pipe(plugins.header(configheader))    				.pipe(plugins.rename({ suffix: '.min' }))
			        .pipe(gulp.dest(taskpath + 'output/'))
	    			.on('error', reject)
			        .on('end', resolve);
			})
        };

        // copy critical-css to storage location
        TASKS['copy'] = function() {
			return new Promise(function(resolve, reject) {

				console.log('\n' + plugins.util.colors.green.bold('Update Critical CSS storage file...'));
				console.log(' ➤ ' + plugins.util.colors.green('/abovethefold/css/global.css'));

				// append extra.css
				gulp.src([taskpath + 'output/critical.min.css'])
    				.pipe(plugins.rename('global.css'))
    				.pipe(plugins.chmod({"owner":{"read":true,"write":true,"execute":false},"group":{"read":true,"write":true,"execute":false},"others":{"read":false,"write":false,"execute":false}})) // WordPress permissions
    				.pipe(plugins.chown(2440,2447)) // set to PHP user
			        .pipe(gulp.dest('css'))
			        .pipe(plugins.es.map(function(file, callback) {
		                plugins.fs.chown(file.path, file.stat.uid, file.stat.gid, callback);
		            }))
	    			.on('error', reject)
			        .on('end', resolve);
			});

        };

        // print size
        TASKS['size'] = function() {
        	return new Promise(function(resolve, reject) {

				console.log('\nCritical CSS processor completed successfully.');

				gulp.src(taskpath + 'output/*')
	    			.pipe(plugins.size( { showFiles: true } ))
	    			.on('error', reject)
			        .on('end', resolve)
			        .pipe(gulp.dest('output', { overwrite: false } ));
			});
        };

        // process optimization tasks
        TASKS['clean']()
        	.then(function() {
	        	return TASKS['critical']()
	        }).then(function() {
	        	return TASKS['concat']()
	        }).then(function() {
	        	return TASKS['minify']()
			}).then(function() {
	        	return TASKS['copy']()
			}).then(function() {
	        	return TASKS['size']()
			}).then(
        		function() {
					cb();
				}
        	);

    };
};
