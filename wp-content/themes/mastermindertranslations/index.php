<?php get_header(); ?>
    
    <!-- baner -->
    <section id="banner">
     <div class="row">
      <div class="container">
       <div class="col-md-4 col-sm-4"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('banner-left')) ?></div>
       <div class="col-md-8 col-sm-8"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('banner-right')) ?></div>
      </div>
     </div>
    </section>
    <!-- /banner -->
    
    <div class="clearfix"></div>
    
    <!-- boxes -->
    <section id="boxes">
     <div class="row">
      <div class="container">
       <div class="col-md-12"><?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('boxes')) ?></div>
      </div>
     </div>
    </section>
    <!-- /boxes -->
    
    <div class="clearfix"></div>
    
    <!-- main -->
    <div class="row">
     <div class="container">
      <section id="main" class="col-md-8 col-sm-8">
       <div class="content-up">
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('main-content-up')) ?>
       </div>

			<h3><?php _e( 'Latest News', 'html5blank' ); ?></h3>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>
            
       <div class="content-down">
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('main-content-down')) ?>
       </div>

	  </section>
      <?php get_sidebar(); ?>
     </div>
    </div>
    <!-- /main -->

<?php get_footer(); ?>
